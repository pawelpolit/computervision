clear;

B = [0.1, 0.2;
     0.2, 0.1;
     0.2, 0.2;
     0.2, 0.3;
     0.3, 0.2;
     0.3, 0.4;
     0.4, 0.3;
     0.4, 0.4;
     0.4, 0.5;
     0.5, 0.4];
 
mu_B = mean(B);
mu_rep = repmat(mu_B, [size(B,1), 1]);
Sigma_B = ((B - mu_rep)'*(B - mu_rep)) / (size(B, 1) - 1);

P1 = [0.3, 0.3];
P2 = [0.22201, 0.23729];
P3 = [0.28699, 0.36441];

%% EXERCISE A: Compute the value of the multivariate Gaussian distribution for P1, P2 and P3 (without using mvnpdf).

n = size(Sigma_B, 1);

disp(1 / ((2 * pi) ^ (n / 2) * sqrt(abs(det(Sigma_B)))) * exp(-1/2 * (P1 - mu_B) * inv(Sigma_B) * (P1 - mu_B)'));
disp(1 / ((2 * pi) ^ (n / 2) * sqrt(abs(det(Sigma_B)))) * exp(-1/2 * (P2 - mu_B) * inv(Sigma_B) * (P2 - mu_B)'));
disp(1 / ((2 * pi) ^ (n / 2) * sqrt(abs(det(Sigma_B)))) * exp(-1/2 * (P3 - mu_B) * inv(Sigma_B) * (P3 - mu_B)'));

%% EXERCISE B: Compute the value of the multivariate Gaussian distribution for P1 (this time use mvnpdf).

disp(mvnpdf(P1, mu_B, Sigma_B));

%% EXERCISE C: Computer covariance matrix with Matlab method.

disp(cov(B));