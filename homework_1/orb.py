# __author__ = 'Pawel Polit'
#
# Homework: Use matching with ORB to match 3 pairs of your images. First pair should return 0 correct matches,
# second pair should return [1-10] correct matches, third pair should return more than 10 correct matches. Look out!
# Images need to be funny!
#
# Points: You can get 5 points (3 points if you get correct number of matches, 2 points if images are funny)
#

from skimage import data
from skimage.feature import (match_descriptors, ORB, plot_matches)
from skimage.color import rgb2gray
import matplotlib.pyplot as plt

img11 = rgb2gray(data.imread("images/a.jpg"))
img12 = rgb2gray(data.imread("images/b.jpg"))

img21 = rgb2gray(data.imread("images/c.jpg"))
img22 = rgb2gray(data.imread("images/d.jpg"))

img31 = rgb2gray(data.imread("images/e.jpg"))
img32 = rgb2gray(data.imread("images/f.jpg"))

descriptor_extractor = ORB(n_keypoints=22)

descriptor_extractor.detect_and_extract(img11)
keypoints11 = descriptor_extractor.keypoints
descriptors11 = descriptor_extractor.descriptors

descriptor_extractor.detect_and_extract(img12)
keypoints12 = descriptor_extractor.keypoints
descriptors12 = descriptor_extractor.descriptors

matches1 = match_descriptors(descriptors11, descriptors12, cross_check=True)

descriptor_extractor.detect_and_extract(img21)
keypoints21 = descriptor_extractor.keypoints
descriptors21 = descriptor_extractor.descriptors

descriptor_extractor.detect_and_extract(img22)
keypoints22 = descriptor_extractor.keypoints
descriptors22 = descriptor_extractor.descriptors

matches2 = match_descriptors(descriptors21, descriptors22, cross_check=True)

descriptor_extractor.detect_and_extract(img31)
keypoints31 = descriptor_extractor.keypoints
descriptors31 = descriptor_extractor.descriptors

descriptor_extractor.detect_and_extract(img32)
keypoints32 = descriptor_extractor.keypoints
descriptors32 = descriptor_extractor.descriptors

matches3 = match_descriptors(descriptors31, descriptors32, cross_check=True)

fig, ax = plt.subplots(nrows=3, ncols=1)

plt.gray()

plot_matches(ax[0], img11, img12, keypoints11, keypoints12, matches1)
ax[0].axis('off')

plot_matches(ax[1], img21, img22, keypoints21, keypoints22, matches2)
ax[1].axis('off')

plot_matches(ax[2], img31, img32, keypoints31, keypoints32, matches3)
ax[2].axis('off')

plt.show()
