# __author__ = 'Pawel Polit'

import cv2

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 600)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 400)

background_subtractor = cv2.createBackgroundSubtractorMOG2()

name = 0
while True:
    ret, frame = cap.read()

    mask = background_subtractor.apply(frame)
    output = cv2.bitwise_and(frame, frame, mask=mask)

    cv2.imwrite('images_without_object/' + str(name) + '.png', output)
    name += 1

    cv2.imshow('frame', output)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
