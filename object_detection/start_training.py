# __author__ = 'Pawel Polit'

import os
import cv2
import random
import numpy as np
from sklearn import ensemble
from sklearn.cluster import KMeans
from sklearn.cross_validation import cross_val_score
from sklearn.externals import joblib
from bic import compute_bic

images_with_object = os.listdir('images_with_object')
images_without_object = os.listdir('images_without_object')

print 'Creating descriptors list...  ',

orb = cv2.ORB_create()
descriptor_list = []
for image_with_object_file in images_with_object:
    image = cv2.imread('images_with_object/' + image_with_object_file, 0)
    kp, des = orb.detectAndCompute(image, None)
    descriptor_list.append(des)
for image_without_object_file in images_without_object:
    image = cv2.imread('images_without_object/' + image_without_object_file, 0)
    kp, des = orb.detectAndCompute(image, None)
    descriptor_list.append(des)

descriptors = np.concatenate(descriptor_list, axis=0)

print 'Done'

print 'Searching for best number of clusters...  ',

clusters = []
bic_value = []
for i in range(2, 60):
    k_means = KMeans(init='k-means++', n_clusters=i, n_init=10)
    test_set = np.array(random.sample(descriptors, 3000))
    k_means.fit(test_set)
    bic_value.append(compute_bic(k_means, test_set))
    clusters.append(i)

number_of_clusters = clusters[bic_value.index(max(bic_value))]

print 'Done, number of clusters: ' + str(number_of_clusters)

print 'Creating k-means data structure...',

k_means = KMeans(init='k-means++', n_clusters=number_of_clusters, n_init=10)
k_means.fit(descriptors)

print 'Done'
print 'Creating list of histograms...'

histogram_list = []
class_list = []

print '     for images with object...  ',

for image_with_object_file in images_with_object:
    image = cv2.imread('images_with_object/' + image_with_object_file, 0)
    kp, des = orb.detectAndCompute(image, None)
    if des is not None:
        histogram_list.append(np.histogram(k_means.predict(des), bins=number_of_clusters)[0])
        class_list.append(1)

print 'Done'

print '     for images without object...  ',

for image_without_object_file in images_without_object:
    image = cv2.imread('images_without_object/' + image_without_object_file, 0)
    kp, des = orb.detectAndCompute(image, None)
    if des is not None:
        histogram_list.append(np.histogram(k_means.predict(des), bins=number_of_clusters)[0])
        class_list.append(0)

print 'Done'

histograms = np.array(histogram_list)
classes = np.array(class_list)

print 'Searching for best number of trees...  ',

scores = []
for i in range(1, 25):
    classifier = ensemble.RandomForestClassifier(n_estimators=i, max_depth=None, min_samples_split=1, random_state=0,
                                                 oob_score=True)
    classifier.fit(histograms, classes)
    score = cross_val_score(classifier, histograms, classes)
    scores.append(score.mean())

number_of_trees = np.array(scores).argmax() + 1

print 'Done, number of trees: ' + str(number_of_trees)

classifier = ensemble.RandomForestClassifier(n_estimators=number_of_trees, max_depth=None, min_samples_split=1,
                                             random_state=0, oob_score=True)
classifier.fit(histograms, classes)

print 'Saving data structures...',

joblib.dump(k_means, 'k_means/k_means.pkl')
joblib.dump(classifier, 'random_forest/random_forest.pkl')

print 'Done'
print 'Training completed successfully'
