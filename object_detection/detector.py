# __author__ = 'Pawel Polit'

import cv2
from sklearn.externals import joblib
import numpy as np

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 600)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 400)

FRAMES_TO_READ = 10

k_means = joblib.load('k_means/k_means.pkl')
number_of_clusters = len(k_means.cluster_centers_)
classifier = joblib.load('random_forest/random_forest.pkl')
orb = cv2.ORB_create()

background_subtractor = cv2.createBackgroundSubtractorMOG2()

string_to_display = ['Nie ma', 'Jest']
string_index = 0
histogram_list = []

while True:
    ret, frame = cap.read()

    mask = background_subtractor.apply(frame)
    output = cv2.bitwise_and(frame, frame, mask=mask)

    if len(histogram_list) < FRAMES_TO_READ:
        kp, des = orb.detectAndCompute(output, None)
        if des is not None:
            histogram_list.append(np.histogram(k_means.predict(des), bins=number_of_clusters)[0])
    else:
        predicted_classes = classifier.predict(histogram_list)
        if sum(predicted_classes) > FRAMES_TO_READ / 2:
            string_index = 1
        else:
            string_index = 0
        histogram_list = []

    cv2.putText(frame, string_to_display[string_index], (10, 50), cv2.FONT_HERSHEY_DUPLEX, 2, (0, 0, 255), 2,
                cv2.LINE_AA)

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
