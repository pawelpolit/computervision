# __author__ = 'Pawel Polit'

import cv2

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 600)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 400)

name = 258
while True:
    ret, frame = cap.read()

    cv2.imwrite('images_with_object_test/' + str(name) + '.png', frame)
    name += 1

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
