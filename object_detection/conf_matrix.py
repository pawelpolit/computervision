# __author__ = 'Pawel Polit'

import os
import cv2
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.externals import joblib

images_with_object_test = os.listdir('images_with_object_test')
images_without_object_test = os.listdir('images_without_object_test')

orb = cv2.ORB_create()
k_means = joblib.load('k_means/k_means.pkl')
classifier = joblib.load('random_forest/random_forest.pkl')

number_of_clusters = len(k_means.cluster_centers_)

histogram_list = []
class_list = []
for with_object_file in images_with_object_test:
    img = cv2.imread('images_with_object_test/' + with_object_file, 0)
    kp, des = orb.detectAndCompute(img, None)
    if des is not None:
        histogram_list.append(np.histogram(k_means.predict(des), bins=number_of_clusters)[0])
        class_list.append(1)

for without_object_file in images_without_object_test:
    img = cv2.imread('images_without_object_test/' + without_object_file, 0)
    kp, des = orb.detectAndCompute(img, None)
    if des is not None:
        histogram_list.append(np.histogram(k_means.predict(des), bins=number_of_clusters)[0])
        class_list.append(0)

histograms = np.array(histogram_list)
classes = np.array(class_list)

predicted_classes = classifier.predict(histograms)
print(confusion_matrix(classes, predicted_classes))
