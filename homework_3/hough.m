%% EXERCISE: Use Hough transform to determine exact locations of the
%           matchedPoints1. You can check if two lines intersect using equations from:
%           https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection

I = imread('card.jpg');
IGray = rgb2gray(I);
edges = edge(IGray, 'log');

[H, theta, rho] = hough(edges);
P = houghpeaks(H, 10);
lines = houghlines(edges, theta, rho, P, 'FillGap', 5, 'MinLength', 50);

[firstBegin,secondBegin,thirdBegin,fourthBegin] = lines.point1;
[firstEnd,secondEnd,thirdEnd,fourthEnd] = lines.point2;

x1 = ( ( firstBegin(1)*firstEnd(2) - firstBegin(2)*firstEnd(1) )*( fourthBegin(1) - fourthEnd(1) ) - ( fourthBegin(1)*fourthEnd(2) - fourthBegin(2)*fourthEnd(1) )*( firstBegin(1) - firstEnd(1) ) ) / ( ( firstBegin(1)-firstEnd(1) ) * ( fourthBegin(2) - fourthEnd(2) ) - ( firstBegin(2)-firstEnd(2) ) * ( fourthBegin(1) - fourthEnd(1) ) );
y1 = ( ( firstBegin(1)*firstEnd(2) - firstBegin(2)*firstEnd(1) )*( fourthBegin(2) - fourthEnd(2) ) - ( fourthBegin(1)*fourthEnd(2) - fourthBegin(2)*fourthEnd(1) )*( firstBegin(2) - firstEnd(2) ) ) / ( ( firstBegin(1)-firstEnd(1) ) * ( fourthBegin(2) - fourthEnd(2) ) - ( firstBegin(2)-firstEnd(2) ) * ( fourthBegin(1) - fourthEnd(1) ) );

x7 = ( ( firstBegin(1)*firstEnd(2) - firstBegin(2)*firstEnd(1) )*( thirdBegin(1) - thirdEnd(1) ) - ( thirdBegin(1)*thirdEnd(2) - thirdBegin(2)*thirdEnd(1) )*( firstBegin(1) - firstEnd(1) ) ) / ( ( firstBegin(1)-firstEnd(1) ) * ( thirdBegin(2) - thirdEnd(2) ) - ( firstBegin(2)-firstEnd(2) ) * ( thirdBegin(1) - thirdEnd(1) ) );
y7 = ( ( firstBegin(1)*firstEnd(2) - firstBegin(2)*firstEnd(1) )*( thirdBegin(2) - thirdEnd(2) ) - ( thirdBegin(1)*thirdEnd(2) - thirdBegin(2)*thirdEnd(1) )*( firstBegin(2) - firstEnd(2) ) ) / ( ( firstBegin(1)-firstEnd(1) ) * ( thirdBegin(2) - thirdEnd(2) ) - ( firstBegin(2)-firstEnd(2) ) * ( thirdBegin(1) - thirdEnd(1) ) );

x3 = ( ( secondBegin(1)*secondEnd(2) - secondBegin(2)*secondEnd(1) )*( fourthBegin(1) - fourthEnd(1) ) - ( fourthBegin(1)*fourthEnd(2) - fourthBegin(2)*fourthEnd(1) )*( secondBegin(1) - secondEnd(1) ) ) / ( ( secondBegin(1)-secondEnd(1) ) * ( fourthBegin(2) - fourthEnd(2) ) - ( secondBegin(2)-secondEnd(2) ) * ( fourthBegin(1) - fourthEnd(1) ) );
y3 = ( ( secondBegin(1)*secondEnd(2) - secondBegin(2)*secondEnd(1) )*( fourthBegin(2) - fourthEnd(2) ) - ( fourthBegin(1)*fourthEnd(2) - fourthBegin(2)*fourthEnd(1) )*( secondBegin(2) - secondEnd(2) ) ) / ( ( secondBegin(1)-secondEnd(1) ) * ( fourthBegin(2) - fourthEnd(2) ) - ( secondBegin(2)-secondEnd(2) ) * ( fourthBegin(1) - fourthEnd(1) ) );

x5 = ( ( secondBegin(1)*secondEnd(2) - secondBegin(2)*secondEnd(1) )*( thirdBegin(1) - thirdEnd(1) ) - ( thirdBegin(1)*thirdEnd(2) - thirdBegin(2)*thirdEnd(1) )*( secondBegin(1) - secondEnd(1) ) ) / ( ( secondBegin(1)-secondEnd(1) ) * ( thirdBegin(2) - thirdEnd(2) ) - ( secondBegin(2)-secondEnd(2) ) * ( thirdBegin(1) - thirdEnd(1) ) );
y5 = ( ( secondBegin(1)*secondEnd(2) - secondBegin(2)*secondEnd(1) )*( thirdBegin(2) - thirdEnd(2) ) - ( thirdBegin(1)*thirdEnd(2) - thirdBegin(2)*thirdEnd(1) )*( secondBegin(2) - secondEnd(2) ) ) / ( ( secondBegin(1)-secondEnd(1) ) * ( thirdBegin(2) - thirdEnd(2) ) - ( secondBegin(2)-secondEnd(2) ) * ( thirdBegin(1) - thirdEnd(1) ) );

x2 = (x1 + x3) / 2;
y2 = (y1 + y3) / 2;

x4 = (x3 + x5) / 2;
y4 = (y3 + y5) / 2;

x6 = (x5 + x7) / 2;
y6 = (y5 + y7) / 2;

x8 = (x7 + x1) / 2;
y8 = (y7 + y1) / 2;



matchedPoints1 = [ x1, y1
%                   x2, y2
                   x3, y3
 %                  x4, y4
                   x5, y5
  %                 x6, y6
                   x7, y7 ];
   %                x8, y8 

matchedPoints2 = [  1,   1
%                   64,   1
                  128,   1
 %                 128, 100
                  128, 200
  %                 64, 200
                    1, 200 ];
   %                 1, 100 

tform = estimateGeometricTransform(matchedPoints1,matchedPoints2,'projective');
Rout = imref2d([200, 128],[0, 128],[0, 200]);
J = imwarp(I, tform, 'OutputView', Rout);

figure;
subplot(1, 2, 1); imshow(I); hold on; plot(matchedPoints1(:, 1), matchedPoints1(:, 2), 'r+');
subplot(1, 2, 2); imshow(J); hold on; plot(matchedPoints2(:, 1), matchedPoints2(:, 2), 'r+');
