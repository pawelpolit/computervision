# __author__ = 'Pawel Polit'

import cv2
import numpy as np
from time import time

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

TARGET_RADIUS = 50

FIRST_TARGET_X = TARGET_RADIUS
FIRST_TARGET_Y = SCREEN_HEIGHT - TARGET_RADIUS

SECOND_TARGET_X = SCREEN_WIDTH - TARGET_RADIUS
SECOND_TARGET_Y = SCREEN_HEIGHT - TARGET_RADIUS


class Ball(object):
    def __init__(self, start_coordinates, radius, color, speed, start_speed_vector):
        self.__coordinates__ = np.array(start_coordinates)
        self.__radius__ = radius
        self.__color__ = color
        self.__speed__ = speed
        self.__speed_vector__ = np.array(start_speed_vector)

        self.__level__ = 0
        self.__change_time__ = 1
        self.__time_point__ = time()

    def draw(self, frame, face):
        current_time = time()

        self.__level_action__(current_time)

        self.__coordinates__ += self.__speed_vector__ * self.__speed__

        is_ok = self.__check_collision__(face)

        cv2.circle(frame, tuple(self.__coordinates__), self.__radius__, self.__color__, -1)

        if not is_ok:
            return False

        if self.__level__ != 2 and current_time - self.__time_point__ >= 10:
            self.__level__ += 1
            self.__change_time__ = 0
            self.__time_point__ = current_time

        return True

    def __level_action__(self, current_time):
        if self.__level__ == 1 and current_time - self.__time_point__ >= self.__change_time__:
            self.__change_time__ += 2
            self.__speed__ += 1

        elif self.__level__ == 2 and current_time - self.__time_point__ >= self.__change_time__:
            self.__change_time__ += 3

            if np.random.randint(2) == 0:
                new_speed_vector = np.random.randint(-3, 4, size=2)

                if new_speed_vector[0] * new_speed_vector[1] == 0:
                    absolute_value = abs(new_speed_vector[0] + new_speed_vector[1])

                    if absolute_value != 0:
                        new_speed_vector /= absolute_value
                        new_speed_vector *= 3

                elif abs(new_speed_vector[0]) == abs(new_speed_vector[1]):
                    new_speed_vector /= abs(new_speed_vector)
                    new_speed_vector *= 3

                self.__speed_vector__ = new_speed_vector

    def __check_collision__(self, face):
        if self.__coordinates__[0] <= self.__radius__:
            self.__speed_vector__[0] *= -1
            self.__coordinates__[0] = self.__radius__

        elif self.__coordinates__[0] >= SCREEN_WIDTH - self.__radius__:
            self.__speed_vector__[0] *= -1
            self.__coordinates__[0] = SCREEN_WIDTH - self.__radius__

        if self.__coordinates__[1] <= self.__radius__:
            self.__speed_vector__[1] *= -1
            self.__coordinates__[1] = self.__radius__

        elif self.__coordinates__[1] >= SCREEN_HEIGHT - self.__radius__:
            self.__speed_vector__[1] *= -1
            self.__coordinates__[1] = SCREEN_HEIGHT - self.__radius__

        if face is not None:
            face_x, face_y, face_radius = face

            distance_between_centers = (face_x - self.__coordinates__[0]) ** 2 + (face_y - self.__coordinates__[1]) ** 2

            if distance_between_centers <= (self.__radius__ + face_radius) ** 2:
                vector_face_to_ball = np.array([self.__coordinates__[0] - face_x, self.__coordinates__[1] - face_y])
                vector_face_to_ball = vector_face_to_ball / np.sqrt(distance_between_centers)
                self.__speed_vector__ = np.rint(3 * vector_face_to_ball).astype(int)

        distance_to_first_target = (FIRST_TARGET_X - self.__coordinates__[0]) ** 2 + \
                                   (FIRST_TARGET_Y - self.__coordinates__[1]) ** 2

        if distance_to_first_target <= (self.__radius__ + TARGET_RADIUS) ** 2:
            return False

        distance_to_second_target = (SECOND_TARGET_X - self.__coordinates__[0]) ** 2 + \
                                    (SECOND_TARGET_Y - self.__coordinates__[1]) ** 2

        if distance_to_second_target <= (self.__radius__ + TARGET_RADIUS) ** 2:
            return False

        return True


def next_frame(cap):
    ret, frame = cap.read()
    return cv2.flip(frame, 1)


def find_and_draw_face(frame):
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    if len(faces) == 0:
        return None

    x, y, w, h = faces[0]
    center_x = x + w / 2
    center_y = y + h / 2
    radius = int(np.sqrt(w ** 2 + h ** 2) / 2.5)
    cv2.circle(frame, (center_x, center_y), radius, (255, 0, 0), 2)

    return center_x, center_y, radius


def draw_targets(frame):
    cv2.circle(frame, (FIRST_TARGET_X, FIRST_TARGET_Y), TARGET_RADIUS, (0, 255, 0), 3)
    cv2.circle(frame, (SECOND_TARGET_X, SECOND_TARGET_Y), TARGET_RADIUS, (0, 255, 0), 3)


def game_is_finished():
    ch = 0xFF & cv2.waitKey(1)
    return ch == 27


def start_game():
    global SCREEN_WIDTH, SCREEN_HEIGHT, FIRST_TARGET_X, FIRST_TARGET_Y, SECOND_TARGET_X, SECOND_TARGET_Y

    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, SCREEN_WIDTH)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, SCREEN_HEIGHT)

    frame_shape = next_frame(cap).shape
    SCREEN_WIDTH = frame_shape[1]
    SCREEN_HEIGHT = frame_shape[0]

    FIRST_TARGET_X = TARGET_RADIUS
    FIRST_TARGET_Y = SCREEN_HEIGHT - TARGET_RADIUS

    SECOND_TARGET_X = SCREEN_WIDTH - TARGET_RADIUS
    SECOND_TARGET_Y = SCREEN_HEIGHT - TARGET_RADIUS

    ball = Ball((SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2), 50, (0, 0, 255), 5, (1, 3))

    start_time = time()

    while not game_is_finished():
        frame = next_frame(cap)

        face = find_and_draw_face(frame)
        draw_targets(frame)

        is_ok = ball.draw(frame, face)

        cv2.putText(frame, 'Time: ' + str(int(time() - start_time)) + 's', (10, 50), cv2.FONT_HERSHEY_DUPLEX, 1,
                    (0, 255, 0), 2, cv2.LINE_AA)

        if not is_ok:
            cv2.putText(frame, 'Game Over', (50, 200), cv2.FONT_HERSHEY_DUPLEX, 3, (0, 0, 255), 2,
                        cv2.LINE_AA)
            cv2.putText(frame, 'Play again (p)', (80, 300), cv2.FONT_HERSHEY_DUPLEX, 2, (0, 0, 255),
                        2, cv2.LINE_AA)

            cv2.imshow('Bounce the Ball', frame)

            if cv2.waitKey(10000) == ord('p'):
                ball = Ball((SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2), 50, (0, 0, 255), 5, (1, 3))
                start_time = time()
                continue

            break

        cv2.imshow('Bounce the Ball', frame)

    cap.release()
    cv2.destroyAllWindows()


start_game()
