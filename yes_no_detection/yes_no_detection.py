# __author__ = 'Pawel Polit'

"""
Lucas-Kanade tracker
====================
Lucas-Kanade sparse optical flow demo. Uses goodFeaturesToTrack
for track initialization and back-tracking for match verification
between frames.
Usage
-----
Keys
    ESC - exit
"""

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2
from common import draw_str

face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

lk_params = dict(winSize=(15, 15),
                 maxLevel=2,
                 criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))  # termination criteria

feature_params = dict(maxCorners=500,
                      qualityLevel=0.3,
                      minDistance=7,
                      blockSize=7)


class App:
    def __init__(self):
        self.track_len = 10
        self.detect_interval = 5
        self.tracks = []
        self.cam = cv2.VideoCapture(0)
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 800)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)
        self.frame_idx = 0

    def __del__(self):
        self.cam.release()

    def __prepare_face_rectangle__(self):
        ret, frame = self.cam.read()
        img = frame

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        if len(faces) > 0:
            self.face_x, self.face_y, self.face_width, self.face_height = faces[0]

    def run(self):

        while True:
            ret, frame = self.cam.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            vis = frame.copy()

            if len(self.tracks) > 0:
                img0, img1 = self.prev_gray, frame_gray
                p0 = np.float32([tr[-1] for tr in self.tracks]).reshape(-1, 1, 2)
                p1, st, err = cv2.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
                p0r, st, err = cv2.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
                d = abs(p0 - p0r).reshape(-1, 2).max(-1)
                good = d < 1
                new_tracks = []
                for tr, (x, y), good_flag in zip(self.tracks, p1.reshape(-1, 2), good):
                    if not good_flag:
                        continue
                    tr.append((x, y))
                    if len(tr) > self.track_len:
                        del tr[0]
                    new_tracks.append(tr)
                    cv2.circle(vis, (x, y), 2, (0, 255, 0), -1)
                self.tracks = new_tracks
                cv2.polylines(vis, [np.int32(tr) for tr in self.tracks], False, (0, 255, 0))
                draw_str(vis, (20, 20), 'track count: %d' % len(self.tracks))

                horizontally = 0
                vertically = 0

                for track in self.tracks:
                    if len(track) == 10:
                        if abs(track[0][0] - track[9][0]) > abs(track[0][1] - track[9][1]) > 7:
                            horizontally += 1
                        elif 7 < abs(track[0][0] - track[9][0]) < abs(track[0][1] - track[9][1]):
                            vertically += 1

                if horizontally > vertically:
                    print('no')
                elif horizontally < vertically:
                    print('yes')

            if self.frame_idx % self.detect_interval == 0:
                mask = np.zeros_like(frame_gray)
                mask[:] = 255
                for x, y in [np.int32(tr[-1]) for tr in self.tracks]:
                    cv2.circle(mask, (x, y), 5, 0, -1)

                self.__prepare_face_rectangle__()

                p = cv2.goodFeaturesToTrack(frame_gray, mask=mask, **feature_params)
                if p is not None:
                    for x, y in np.float32(p).reshape(-1, 2):
                        if self.face_x <= x <= self.face_x + self.face_width and \
                                                self.face_y <= y <= self.face_y + self.face_height:
                            self.tracks.append([(x, y)])

            self.frame_idx += 1
            self.prev_gray = frame_gray
            cv2.rectangle(vis, (self.face_x, self.face_y),
                          (self.face_x + self.face_width, self.face_y + self.face_height), (255, 0, 0), 2)
            cv2.imshow('lk_track', vis)

            ch = 0xFF & cv2.waitKey(1)
            if ch == 27:
                break


def main():
    print(__doc__)
    App().run()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
